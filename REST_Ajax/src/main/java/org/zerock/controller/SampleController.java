
package org.zerock.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zerock.domain.SampleVO;
import org.zerock.domain.Ticket;

import lombok.extern.log4j.Log4j;

@RestController
@RequestMapping("/sample")
@Log4j
public class SampleController {

	@GetMapping(value = "/getText", produces = "text/plain; charset=UTF-8")
	public String getText() {

		log.info("MIME TYPE: " + MediaType.TEXT_PLAIN_VALUE);

		return "안녕하세요";

	}

	@GetMapping(value = "/getSample", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE })
	// 스프링 5.2부터는 APPLICATION_JSON_VALUE
	public SampleVO getSample()
	{
		return new SampleVO(112, "스타", "로드");
	}

	@GetMapping(value = "/getSample2")
	public SampleVO getSample2() {
		return new SampleVO(113, "로켓", "라쿤");
	}

	@GetMapping(value = "/getList")
	public List<SampleVO> getList()
	{
		return IntStream.range(1, 10).mapToObj(i -> new SampleVO(i, i + "First", i + " Last"))
				.collect(Collectors.toList());
	}

	@GetMapping(value = "/getMap")
	public Map<String, SampleVO> getMap()
	{
		Map<String, SampleVO> map = new HashMap<>();
		map.put("First", new SampleVO(111, "그루트", "주니어"));

		return map;
	}

	@GetMapping(value = "/check", params = { "height", "weight" })
	public ResponseEntity<SampleVO> check(Double height, Double weight)
	{
		SampleVO vo = new SampleVO(000, "" + height, "" + weight);

		ResponseEntity<SampleVO> result = null;

		if (height < 150) {
			result = ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(vo);
		} else {
			result = ResponseEntity.status(HttpStatus.OK).body(vo);
		}

		return result;
	}

	@GetMapping("/product/{cat}/{pid}")
	// 일반 컨트롤러에서도 사용이 가능하지만 REST 방식에서 자주 사용됩니다. URL 경로의 일부를 파라미터로 사용할 때 이용
	// 값을 얻을 때에는 int, double과 같은 기본 자료형은 사용할 수 없다.
	public String[] getPath(@PathVariable("cat") String cat, @PathVariable("pid") Integer pid)
	{
		return new String[] { "category: " + cat, "productid: " + pid };
	}

	@PostMapping("/ticket")
	// JSON 데이터를 원하는 타입의 객체로 변환해야 하는 경우에 주로 사용
	public Ticket convert(@RequestBody Ticket ticket)
	{
		log.info("convert.......ticket" + ticket);

		return ticket;
	}

}
