<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>


<form action="uploadFormAction" method="post" enctype="multipart/form-data">

	<input type='file' name='uploadFile' multiple>
	<!-- enctype의 속성값을 'multipart/form-data'로 지정하는 것 -->
	<!-- <input type='file'> 의 경우 최근 브라우저에서는  'multiple'이라는 속성을 지원하는데 이는 하나의 <input> 태그로 한꺼번에 여러 개의 파일을 업로드
			할수 있다. IE의 경우 10 이상에서만 사용 가능 -->
	<button>Submit</button>

</form>

</body>
</html>
