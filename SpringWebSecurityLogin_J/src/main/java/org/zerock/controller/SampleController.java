package org.zerock.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.log4j.Log4j;



@Log4j
@RequestMapping("/sample/*")
@Controller
public class SampleController
{
	@GetMapping("/all")
	public void doAll()
	{
		log.info("do all can access everybody");
	}

	@GetMapping("/member")
	public void doMember()
	{
		log.info("logined member");
	}

	@GetMapping("/admin")
	public void doAdmin()
	{
		log.info("admin only");
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MEMBER')")
	// @PreAuthorize, @PostAuthorize
	// 3버전부터 지원되며, () 안에 표현식을 사용할 수 있으므로 최근에는 더 많이 사용됩니다.
	@GetMapping("/annoMember")
	public void doMember2()
	{
		log.info("logined annotation member");
	}

	@Secured({"ROLE_ADMIN"})
	// 스프링 시큐리티 초기부터 사용되었고, () 안에 'ROLE_ADMIN'과 같은 문자열 혹은 문자열 배열을 이용합니다.
	// 단순히 값만을 추가할 수 있으므로 여러 개를 사용할 때에는 배열로 표현합니다.
	@GetMapping("/annoAdmin")
	public void doAdmin2()
	{
		log.info("admin annotaion only");
	}
}
